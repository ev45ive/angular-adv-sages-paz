import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { PlaylistsRoutingModule } from './playlists-routing.module';
import { PlaylistsComponent } from './playlists.component';
import { SharedModule } from '../shared/shared.module';
import { PlaylistsViewContainer } from './containers/playlists-view/playlists-view.container';
import { PlaylistsListComponent } from './components/playlists-list/playlists-list.component';
import { PlaylistsDetailsComponent } from './components/playlists-details/playlists-details.component';
import { PlaylistsEditorComponent } from './components/playlists-editor/playlists-editor.component';
@NgModule({
  declarations: [
    PlaylistsComponent,
    PlaylistsViewContainer,
    PlaylistsListComponent,
    PlaylistsDetailsComponent,
    PlaylistsEditorComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    PlaylistsRoutingModule
  ],
  providers: [
 
  ]
})
export class PlaylistsModule { }
