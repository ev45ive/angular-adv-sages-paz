import { Component, Inject, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Select, Store } from '@ngxs/store';
import { Playlist } from "apps/sages/src/app/core/model/Playlist";
import { Observable } from 'rxjs';
import { filter, shareReplay, switchMap } from 'rxjs/operators';
import { Playlists } from '../../../core/actions/playlist.actions';
import { PlaylistsApiService } from '../../../core/api/playlists-api.service';
import { PlaylistsState } from '../../../core/state/playlist.store';
import { INITIAL_PLAYLISTS } from '../../../core/tokens';
import { PlaylistsService } from '../../services/playlists.service';

type ViewMode = 'details' | 'edit' | 'create';

@Component({
  selector: 'sages-playlists-view',
  templateUrl: './playlists-view.container.html',
  styleUrls: ['./playlists-view.container.scss']
})
export class PlaylistsViewContainer implements OnInit {

  mode: ViewMode = 'details'
  selected = this.route.queryParams.pipe(
    filter(params => params.id),
    switchMap(params => this.service.fetchPlaylistById(params.id)),
    shareReplay()
  )

  @Select(PlaylistsState.allPlaylists)
  playlists!: Observable<Playlist[]>

  select(playlistId: Playlist['id']) {
    this.router.navigate([], {
      queryParams: { id: playlistId },
      relativeTo: this.route
    })
  }

  constructor(
    private store: Store,
    private service: PlaylistsApiService,
    private router: Router,
    private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.store.dispatch(new Playlists.FetchAll()).subscribe()
  }

  editMode() { this.mode = 'edit' }
  cancel() {
    this.mode = 'details'
  }

  updatePlaylist(draft: Playlist) {

    this.store.dispatch(new Playlists.Update(draft)).subscribe(() => {

      this.selected = this.route.queryParams.pipe(
        switchMap(params => this.service.fetchPlaylistById(params.id)),
        shareReplay()
      )
    })

  }

  createMode() { this.mode = 'create' }

}
