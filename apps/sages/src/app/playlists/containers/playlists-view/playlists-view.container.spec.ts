import { Component } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { playlistsMock } from 'apps/sages/src/stories/playlists/playlistsMock';
import { Subject } from 'rxjs';
import { PlaylistsService } from '../../services/playlists.service';

import { PlaylistsViewContainer } from './playlists-view.container';

@Component({
  selector: 'sages-playlists-list',
  inputs: ['playlists'],
  template: ''
})
class PlaylistsListComponent { }

describe.only('PlaylistsViewContainer', () => {
  let component: PlaylistsViewContainer;
  let fixture: ComponentFixture<PlaylistsViewContainer>;

  beforeEach(async () => {
    const mock = jest.fn().mockImplementation(() => ({
      playlistsChange: new Subject(),
      savePlaylist: jest.fn(),
      getPlaylistById: jest.fn(),
    }))
    await TestBed.configureTestingModule({
      declarations: [PlaylistsViewContainer, PlaylistsListComponent],
      providers: [
        {
          provide: PlaylistsService,
          useFactory: mock
        }
      ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PlaylistsViewContainer);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it.only('should render playlists', () => {
    const mock = TestBed.inject(PlaylistsService)
    mock.playlistsChange.next(playlistsMock)
    fixture.detectChanges()
    const listCmp = fixture.debugElement.query(By.directive(PlaylistsListComponent))


    expect(listCmp.componentInstance.playlists).toEqual(playlistsMock)
  });
});
