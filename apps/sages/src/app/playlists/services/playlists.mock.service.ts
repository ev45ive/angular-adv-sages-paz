import { EventEmitter, Inject, Injectable } from '@angular/core';
import { BehaviorSubject, concat, of, ReplaySubject, Subject } from 'rxjs';
import { tap } from 'rxjs/operators';
import { Playlist } from '../../core/model/Playlist';
import { INITIAL_PLAYLISTS } from '../../core/tokens';

@Injectable({
    providedIn: 'root'
})
export class PlaylistsMockService {

    constructor(
        @Inject(INITIAL_PLAYLISTS) public initial: Playlist[]
    ) { }

    private playlists = new BehaviorSubject<Playlist[]>(this.initial)
    public playlistsChange = this.playlists.asObservable().pipe(
        // tap((playlists) => {
        //     // send stale, while revalidate (send request)
        // })
    )

    getPlaylists() {
        return this.playlistsChange
    }

    getPlaylistById(id: Playlist['id']) {
        return of(this.playlists.getValue().find(p => p.id == id))
    }

    savePlaylist(draft: Playlist) {
        const playlists = this.playlists.getValue().map(p => p.id === draft.id ? draft : p)
        this.playlists.next(playlists)

        return of(draft)
    }
}
