import { EventEmitter, Inject, Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Playlist } from '../../core/model/Playlist';
import { INITIAL_PLAYLISTS } from '../../core/tokens';

@Injectable({
  providedIn: 'root'
})
export class PlaylistsService {

  constructor(
    @Inject(INITIAL_PLAYLISTS) public playlists: Playlist[]
  ) { }

  
  playlistsChange = new EventEmitter<Playlist[]>()

  getPlaylists(): Observable<Playlist[]> {
    throw 'Not Implemented'
  }

  getPlaylistById(id: Playlist['id']): Observable<Playlist | undefined> {
    throw 'Not Implemented'
  }

  savePlaylist(draft: Playlist): Observable<Playlist> {
    throw 'Not Implemented'
  }
}
