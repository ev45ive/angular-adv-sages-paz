import { CUSTOM_ELEMENTS_SCHEMA, DebugElement } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatListModule } from '@angular/material/list';
import { By } from '@angular/platform-browser';
import { playlistsMock } from 'apps/sages/src/stories/playlists/playlistsMock';
import { SharedModule } from '../../../shared/shared.module';

import { PlaylistsDetailsComponent } from './playlists-details.component';

describe('PlaylistsDetailsComponent', () => {
  let component: PlaylistsDetailsComponent;
  let fixture: ComponentFixture<PlaylistsDetailsComponent>;
  let elem: DebugElement

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PlaylistsDetailsComponent],
      // imports: [MatListModule, SharedModule],
      schemas: [CUSTOM_ELEMENTS_SCHEMA], // Shallow rendering
      providers: []
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PlaylistsDetailsComponent);
    component = fixture.componentInstance;
    elem = fixture.debugElement
    component.playlist = playlistsMock[0]
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render playlist details', () => {
    const playlistname = elem.query(By.css('[data-testid=playlist_name]'));
    expect(playlistname.nativeElement.textContent).toMatch(playlistsMock[0].name);
  });

  it('should emit event when button clicked', () => {
    const spy = jest.fn() // jasmine.spy()
    component.onEdit.subscribe(spy)

    const btn = elem.query(By.css('button'));
    btn.triggerEventHandler('click', { /* bubbles: true  */})

    expect(spy).toHaveBeenCalledTimes(1)
  })
});
