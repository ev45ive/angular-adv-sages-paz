import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Playlist } from '../../../core/model/Playlist';

@Component({
  selector: 'sages-playlists-details',
  templateUrl: './playlists-details.component.html',
  styleUrls: ['./playlists-details.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PlaylistsDetailsComponent implements OnInit {

  /**
   * Playlist data
   */
  @Input() playlist!: Playlist
  @Output() onEdit = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
    if (!this.playlist) { throw new Error('Missing Input playlist') }
  }

}
