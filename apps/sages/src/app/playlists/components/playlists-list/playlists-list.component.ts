import { NgForOf, NgForOfContext, NgIf, NgIfContext } from '@angular/common';
import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Playlist } from '../../../core/model/Playlist';

NgIf
NgIfContext
NgForOf
NgForOfContext

// import { MatCellDef, MatColumnDef, MatTable } from '@angular/material/table'

@Component({
  selector: 'sages-playlists-list',
  templateUrl: './playlists-list.component.html',
  styleUrls: ['./playlists-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PlaylistsListComponent implements OnInit {

  @Input() playlists: Playlist[] = [];
  @Output() selectedChange = new EventEmitter<Playlist['id']>();
  @Input() selected?: string;

  select(playlist: Playlist) {
    // this.selected = playlist.id
    this.selectedChange.emit(playlist.id)
  }

  constructor() { }

  ngOnInit(): void {
  }

}
