import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
// import { Route } from '@angular/compiler/src/core';
import { RouterModule, Routes } from '@angular/router';
import { CoreModule } from './core/core.module';
import { SharedModule } from './shared/shared.module';
import { Route } from '@angular/compiler/src/core';
import { playlistsMock } from '../stories/playlists/playlistsMock';
import { environment } from '../environments/environment';
import { INITIAL_PLAYLISTS } from './core/tokens';
import { PlaylistsService } from './playlists/services/playlists.service';
import { PlaylistsMockService } from './playlists/services/playlists.mock.service';
import { Playlist } from './core/model/Playlist';
import { ISearchService } from './browse-music/services/ISearchService';
import { MusicSearchMockService } from './browse-music/services/music-search-mock.service';

const routes: Routes = [
  { path: 'playlists', loadChildren: () => import('./playlists/playlists.module').then(m => m.PlaylistsModule) },
  { path: 'music', loadChildren: () => import('./browse-music/browse-music.module').then(m => m.BrowseMusicModule) }
]

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    CoreModule,
    RouterModule.forRoot(routes, {}),
    SharedModule,
    // environment.production ? [] : [MocksModule]
  ],
  providers: [
    environment.production ? [] : [{
      provide: INITIAL_PLAYLISTS, useValue: playlistsMock
    }],
    // {
    //   provide: PlaylistsService,
    //   // useValue: new PlaylistsMockService(playlistsMock),
    //   useFactory(data: Playlist[]) {
    //     return new PlaylistsMockService(data)//.initilize().configure()...
    //   }, deps: [INITIAL_PLAYLISTS]
    // },
    {
      provide: PlaylistsService,
      useClass: PlaylistsMockService
    },
    // {
    //   provide: ISearchService,
    //   useClass: MusicSearchMockService
    // }
  ],
  bootstrap: [AppComponent],
})
export class AppModule { }
