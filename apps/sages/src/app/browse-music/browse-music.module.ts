import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { BrowseMusicRoutingModule } from './browse-music-routing.module';
import { BrowseMusicComponent } from './browse-music.component';
import { SharedModule } from '../shared/shared.module';
import { AlbumSearchComponent } from './containers/album-search/album-search.component';
import { SearchFormComponent } from './components/search-form/search-form.component';
import { SearchResultsComponent } from './components/search-results/search-results.component';
import { AlbumCardComponent } from './components/album-card/album-card.component';
import { AlbumDetailsComponent } from './containers/album-details/album-details.component';

const routes: Routes = [
  { path: '', component: BrowseMusicComponent }
];

@NgModule({
  declarations: [
    BrowseMusicComponent,
    AlbumSearchComponent,
    SearchFormComponent,
    SearchResultsComponent,
    AlbumCardComponent,
    AlbumDetailsComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    BrowseMusicRoutingModule,
    RouterModule.forChild(routes)
  ]
})
export class BrowseMusicModule { }
