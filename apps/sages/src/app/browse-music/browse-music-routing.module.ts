import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BrowseMusicComponent } from './browse-music.component';
import { AlbumDetailsComponent } from './containers/album-details/album-details.component';
import { AlbumSearchComponent } from './containers/album-search/album-search.component';

const routes: Routes = [{
  path: '', component: BrowseMusicComponent,
  children: [
    {
      path: '', pathMatch: 'full', redirectTo: 'search'
    },
    {
      path: 'search', component: AlbumSearchComponent
    },
    {
      path: 'albums', component: AlbumDetailsComponent
    }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BrowseMusicRoutingModule { }
