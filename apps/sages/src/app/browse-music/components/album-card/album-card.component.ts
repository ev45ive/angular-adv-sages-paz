import { Component, OnInit, ChangeDetectionStrategy, Input } from '@angular/core';
import { AlbumView } from '../../../core/model/album';

@Component({
  selector: 'sages-album-card',
  templateUrl: './album-card.component.html',
  styleUrls: ['./album-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AlbumCardComponent implements OnInit {

  @Input() album!: AlbumView


  constructor() { }

  ngOnInit(): void {
  }

}
