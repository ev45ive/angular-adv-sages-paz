import { Component, OnInit, ChangeDetectionStrategy, Input } from '@angular/core';
import { AlbumView } from '../../../core/model/album';

@Component({
  selector: 'sages-search-results',
  templateUrl: './search-results.component.html',
  styleUrls: ['./search-results.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SearchResultsComponent implements OnInit {

  @Input()
  results: AlbumView[] | null = [];

  constructor() { }

  ngOnInit(): void {
  }

}
