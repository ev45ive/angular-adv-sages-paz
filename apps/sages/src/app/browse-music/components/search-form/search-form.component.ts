import { Component, OnInit, ChangeDetectionStrategy, EventEmitter, Output, Input } from '@angular/core';

@Component({
  selector: 'sages-search-form',
  templateUrl: './search-form.component.html',
  styleUrls: ['./search-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SearchFormComponent implements OnInit {

  @Input() query: string | null = ''

  @Output() search = new EventEmitter<string>();

  submit(query: string) {
    this.search.emit(query)
  }

  constructor() { }

  ngOnInit(): void {
  }

}
