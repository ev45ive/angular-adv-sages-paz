import { Component, OnInit, ChangeDetectionStrategy, Inject } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BehaviorSubject, ConnectableObservable, from, iif, NEVER, of, pipe, ReplaySubject, Subject, Subscription } from 'rxjs';
import { filter, map, multicast, publish, publishBehavior, publishReplay, refCount, share, shareReplay, switchMap, takeUntil, takeWhile, tap } from 'rxjs/operators';
import { Album } from '../../../core/model/album';
import { ISearchService } from '../../services/ISearchService';
import { MusicSearchService } from '../../services/music-search.service';

const isNotNull = pipe(filter((q): q is string => q !== null))
@Component({
  selector: 'sages-album-search',
  templateUrl: './album-search.component.html',
  styleUrls: ['./album-search.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AlbumSearchComponent implements OnInit {
  query = this.service.queryChange
  // results = this.service.resultsChange

  results = this.route.queryParamMap.pipe(
    map(queryParamMap => queryParamMap.get('q')),
    isNotNull,
    // tap(console.log),
    // tap( (..args) => {debugger})
    switchMap(q => this.service.search(q))
  )

  message = this.service.messages

  constructor(
    @Inject(ISearchService)
    // private service: ISearchService<Album, string>
    private service: MusicSearchService,
    private router: Router,
    private route: ActivatedRoute,
  ) { }

  search(query: string) {
    // this.router.navigate(['/music', 'search'], {
    // this.router.navigate(['..','details'], {
    this.router.navigate([], {
      queryParams: { q: query },
      relativeTo: this.route,
      replaceUrl:true // only last search result in history
    })
  }


  ngOnInit(): void {

    // this.route.queryParamMap.pipe(
    //   map(queryParamMap => queryParamMap.get('q')),
    //   isNotNull,
    // )
    // // .subscribe(this.service.query)
    // .subscribe(q => this.service.search(q))
  }
}
