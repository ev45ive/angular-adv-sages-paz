import { Component, OnInit, ChangeDetectionStrategy, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BehaviorSubject, ReplaySubject } from 'rxjs';
import { map, pluck, share, shareReplay, switchMap, tap } from 'rxjs/operators';
import { SearchApiService } from '../../../core/api/search-api.service';
import { Track } from '../../../core/model/album';


// TODO:

// enter http://localhost:4200/music/albums?id=5Tby0U5VndHW0SomYO7Id7
// show album id from id query param in template
// use api service to fetch album
// show album card with album data 
// show exttra album data 

// add link/redirect from search results / album card

@Component({
  selector: 'sages-album-details',
  templateUrl: './album-details.component.html',
  styleUrls: ['./album-details.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AlbumDetailsComponent implements OnInit {

  album_id = this.route.queryParams.pipe(pluck('id'))
  album = this.album_id.pipe(
    switchMap(id => this.api.fetchAlbumById(id)),
    // tap(console.log),
    shareReplay()
  )
  tracks = this.album.pipe(
    map(album => album.tracks?.items || []),
    tap(() => this.setupPlayer())
  )
  currentTrack = new ReplaySubject<Track>()

  @ViewChild('audioRef', { static: false }) audioRef?: ElementRef<HTMLAudioElement>


  constructor(
    private api: SearchApiService,
    private route: ActivatedRoute,
    private router: Router) { }

  playTrack(track: Track) {
    this.currentTrack.next(track)
    setTimeout(() => {
      this.audioRef?.nativeElement.play()
    })
  }
  
  setupPlayer() {
    setTimeout(() => {
      const audio = this.audioRef?.nativeElement
      if (audio) {
        audio.volume = 0.05
      }
    })
  }

  ngAfterViewInit(): void { }

  ngOnInit(): void {
  }

}
