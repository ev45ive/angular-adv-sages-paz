import { InjectionToken } from '@angular/core';
import { Observable } from 'rxjs';


export interface ISearchService<T, Q> {
    queryChange: Observable<Q>;
    resultsChange: Observable<T[]>;
    search(query: Q): Observable<T[]>;
}

export const ISearchService = new InjectionToken<ISearchService<any, any>>('ISearchService')