import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { BehaviorSubject, EMPTY, Observable, of, ReplaySubject, Subject, throwError } from 'rxjs';
import { catchError, debounceTime, filter, first, map, pluck, shareReplay, skip, startWith, switchMap } from 'rxjs/operators';
import { SearchApiService } from '../../core/api/search-api.service';
import { Album } from '../../core/model/album';
import { API_URL } from '../../core/tokens';
import { ISearchService } from './ISearchService';

@Injectable({
  providedIn: 'root'
})
export class MusicSearchService implements ISearchService<Album, string>{

  constructor(
    private api: SearchApiService
  ) { }

  messages = new Subject<string>()

  private query = new ReplaySubject<string>(5)
  queryChange = this.query.asObservable()

  resultsChange = this.query.pipe(
    debounceTime(1),
    switchMap(query => this.fetchSearchResults(query)),
    startWith([]),
    shareReplay()
  )

  search(query: string): Observable<Album[]> {
    this.query.next(query)

    return this.resultsChange // .pipe(skip(1), first())
  }

  fetchSearchResults(query: string): Observable<Album[]> {
    return this.api.fetchSearch({ query })
      .pipe(
        map(res => res.albums.items),
        catchError(err => {
          this.messages.next(err.message);
          return EMPTY;
        })
      );
  }
}
