import { Component } from '@angular/core';

@Component({
  selector: 'sages-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'MusicApp';
  config = { clockSetting: 1 }

  ngDoCheck(): void {
    //Called every time that the input properties of a component or a directive are checked. Use it to extend change detection by performing a custom check.
    //Add 'implements DoCheck' to the class.
    console.log('app: detecting changes')
  }
}
