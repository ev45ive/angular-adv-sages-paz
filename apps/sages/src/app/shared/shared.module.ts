import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavigationComponent } from './containers/navigation/navigation.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { FlexLayoutModule } from '@angular/flex-layout';
import { CenterLayoutComponent } from './containers/center-layout/center-layout.component';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatCardModule } from '@angular/material/card';
import { MatMenuModule } from '@angular/material/menu';
import { FormsModule } from '@angular/forms';
import { ClockComponent } from './widgets/clock/clock.component';
import { RecentSearchesComponent } from './widgets/recent-searches/recent-searches.component';
import { RouterModule } from '@angular/router';
import { TracksListComponent } from './components/tracks-list/tracks-list.component';
import { UiToolkitModule } from '@sages/ui';

@NgModule({
  declarations: [NavigationComponent, CenterLayoutComponent, ClockComponent, RecentSearchesComponent, TracksListComponent],
  imports: [
    UiToolkitModule,
    CommonModule,
    LayoutModule,
    FlexLayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatFormFieldModule,
    MatInputModule,
    MatCheckboxModule,
    MatCardModule,
    MatMenuModule,
    FormsModule,
    RouterModule,
  ],
  exports: [
    UiToolkitModule,
    RouterModule,
    NavigationComponent,
    CenterLayoutComponent,

    FlexLayoutModule,
    MatToolbarModule,
    MatButtonModule,

    MatIconModule,
    MatListModule,
    MatFormFieldModule,
    MatInputModule,
    MatCheckboxModule,
    MatCardModule,
    MatMenuModule,
    FormsModule,
    ClockComponent,
    RecentSearchesComponent,
    TracksListComponent
  ]
})
export class SharedModule { }
