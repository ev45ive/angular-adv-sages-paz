import { Component, OnInit, ChangeDetectionStrategy, Input, EventEmitter, Output } from '@angular/core';
import { Track } from '../../../core/model/album';

@Component({
  selector: 'sages-tracks-list',
  templateUrl: './tracks-list.component.html',
  styleUrls: ['./tracks-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TracksListComponent implements OnInit {

  @Input() tracks: Track[] = [];
  @Output() play = new EventEmitter<Track>();

  constructor() { }

  ngOnInit(): void {
  }

}
