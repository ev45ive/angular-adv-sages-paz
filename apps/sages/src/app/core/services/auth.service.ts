import { Injectable } from '@angular/core';

import { OAuthService } from 'angular-oauth2-oidc';
import { environment } from 'apps/sages/src/environments/environment';
import { from } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private oauth: OAuthService
  ) {
    this.oauth.configure(environment.authConfig)
  }

  init() {
    from(this.oauth.tryLoginImplicitFlow()).subscribe(() => {
      if (!this.getToken()) {
        this.login()
      }
    })
  }

  login() {
    this.oauth.initLoginFlow()
  }

  logout() {
    this.oauth.logOut()
  }

  getToken() {
    return this.oauth.getAccessToken()
  }
}
