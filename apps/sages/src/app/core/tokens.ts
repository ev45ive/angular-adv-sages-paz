import { InjectionToken } from '@angular/core';
import { Playlist } from './model/Playlist';

export const INITIAL_PLAYLISTS = new InjectionToken<Playlist[]>('INITIAL_PLAYLISTS');
export const API_URL = new InjectionToken<string>('API_URL');
