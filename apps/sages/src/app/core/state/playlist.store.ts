import { Injectable } from "@angular/core";
import { State, Action, StateContext, Selector } from "@ngxs/store";
import { playlistsMock } from "apps/sages/src/stories/playlists/playlistsMock";
import { tap } from "rxjs/operators";
import { Playlists } from "../actions/playlist.actions";
import { PlaylistsApiService } from "../api/playlists-api.service";
import { Playlist } from "../model/Playlist";

interface PlaylistsModel {
    items: Playlist[]
    selected?: Playlist
}

@State<PlaylistsModel>({
    name: 'playlists',
    defaults: {
        items: playlistsMock,
    }
})
@Injectable()
export class PlaylistsState {

    constructor(private api: PlaylistsApiService) { }

    @Selector() 
    static allPlaylists(state: PlaylistsModel) {
        return state.items
    }

    @Action(Playlists.FetchAll)
    fetchPlaylists(ctx: StateContext<PlaylistsModel>) {
        const state = ctx.getState();

        return this.api.fetchPlaylists().pipe(
            tap(items => {
                ctx.setState({ ...state, items });
            })
        )
    }
}