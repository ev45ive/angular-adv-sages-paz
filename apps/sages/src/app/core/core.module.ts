import { HttpClient, HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { Inject, NgModule } from '@angular/core';
import { environment } from '../../environments/environment';
import { ISearchService } from '../browse-music/services/ISearchService';
import { MusicSearchService } from '../browse-music/services/music-search.service';
import { API_URL, INITIAL_PLAYLISTS } from './tokens';
import { OAuthModule } from 'angular-oauth2-oidc';
import { AuthService } from './services/auth.service';
import { AuthInterceptor } from './interceptors/auth.interceptor';
import { NgxsModule } from '@ngxs/store';
import { PlaylistsState } from './state/playlist.store';


@NgModule({
  declarations: [],
  imports: [
    HttpClientModule,
    OAuthModule.forRoot({
      resourceServer: {
        sendAccessToken: true,
        allowedUrls: [environment.api_url],
        // customUrlValidation(){}
      }
    }) //  ModuleWithProviders<OAuthModule>
    ,
    NgxsModule.forRoot([
      PlaylistsState
    ], { developmentMode: !environment.production })
  ],
  providers: [
    // Cannot mix multi providers and regular providers
    {
      provide: HTTP_INTERCEPTORS, // InjectionToken<HttpInterceptor[]>
      useClass: AuthInterceptor,
      multi: true
    },
    {
      provide: API_URL, useValue: environment.api_url
    },
    // HttpClient,
    // {
    //   provide: HttpHandler, useClass: MySuperExtraBetterHttpHandler 
    // },
    {
      provide: INITIAL_PLAYLISTS, useValue: []
    },
    {
      provide: ISearchService,
      useClass: MusicSearchService
    }
  ]
})
export class CoreModule {
  constructor(
    private auth: AuthService,
    // @Inject(HTTP_INTERCEPTORS) interceptors: any,
    // @Inject(ROUTES) routes: any,
  ) {
    // debugger
    this.auth.init()
  }
}
