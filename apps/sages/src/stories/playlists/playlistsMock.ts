import { Playlist } from '../../app/core/model/Playlist';


export const playlistsMock: Playlist[] = [
    { id: '123', name: 'Playlist 123', public: false, description: '' },
    { id: '234', name: 'Playlist 234', public: true, description: '' },
    { id: '345', name: 'Playlist 345', public: false, description: '' },
];
