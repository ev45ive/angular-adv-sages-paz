// also exported from '@storybook/angular' if you can deal with breaking changes in 6.1
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { moduleMetadata } from '@storybook/angular';
import { Story, Meta } from '@storybook/angular/types-6-0';
import { PlaylistsEditorComponent } from '../../app/playlists/components/playlists-editor/playlists-editor.component';
import { SharedModule } from '../../app/shared/shared.module';

import '../../styles.scss'

// More on default export: https://storybook.js.org/docs/angular/writing-stories/introduction#default-export
export default {
    title: 'Playlists/Playlists Editor',
    component: PlaylistsEditorComponent,
    // More on argTypes: https://storybook.js.org/docs/angular/api/argtypes
    argTypes: {
        // backgroundColor: { control: 'color' },
    },
    decorators: [
        moduleMetadata({
            imports: [
                BrowserAnimationsModule,
                SharedModule
            ],
        }),
    ],
} as Meta;

// More on component templates: https://storybook.js.org/docs/angular/writing-stories/introduction#using-args
const Template: Story<PlaylistsEditorComponent> = (args: PlaylistsEditorComponent) => ({
    props: args,
});

export const Editing_Playlist = Template.bind({});
// More on args: https://storybook.js.org/docs/angular/writing-stories/args
Editing_Playlist.args = {
    playlist: {
        id: '123', name: 'Playlist', public: false, description: '',
    }
};

export const Empty_Creating_Playlist = Template.bind({});
// More on args: https://storybook.js.org/docs/angular/writing-stories/args
Empty_Creating_Playlist.args = {
    
};
