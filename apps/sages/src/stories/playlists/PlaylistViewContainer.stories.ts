// also exported from '@storybook/angular' if you can deal with breaking changes in 6.1
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { moduleMetadata } from '@storybook/angular';
import { Story, Meta } from '@storybook/angular/types-6-0';
import { INITIAL_PLAYLISTS } from '../../app/core/tokens';
import { PlaylistsDetailsComponent } from '../../app/playlists/components/playlists-details/playlists-details.component';
import { PlaylistsEditorComponent } from '../../app/playlists/components/playlists-editor/playlists-editor.component';
import { PlaylistsListComponent } from '../../app/playlists/components/playlists-list/playlists-list.component';
import { PlaylistsViewContainer } from '../../app/playlists/containers/playlists-view/playlists-view.container';
import { PlaylistsModule } from '../../app/playlists/playlists.module';
import { SharedModule } from '../../app/shared/shared.module';

import '../../styles.scss'
import PlaylistEditorStories from './PlaylistEditor.stories';
import { playlistsMock } from './playlistsMock';



const defaultModule = {
    declarations: [
        PlaylistsListComponent,
        PlaylistsDetailsComponent,
        PlaylistsEditorComponent
    ],

    imports: [
        BrowserAnimationsModule,
        SharedModule
    ],
};
// More on default export: https://storybook.js.org/docs/angular/writing-stories/introduction#default-export
export default {
    title: 'Pages/Playlists',
    component: PlaylistsViewContainer,
    // More on argTypes: https://storybook.js.org/docs/angular/api/argtypes
    argTypes: {
        // backgroundColor: { control: 'color' },
    },
    // decorators: [
    //     moduleMetadata(defaultModule),
    // ],
} as Meta;

// More on component templates: https://storybook.js.org/docs/angular/writing-stories/introduction#using-args
const Template: Story<PlaylistsViewContainer> = (args: PlaylistsViewContainer) => ({
    props: args,
});

export const Primary = Template.bind({});
// More on args: https://storybook.js.org/docs/angular/writing-stories/args
Primary.args = {};
Primary.decorators = [
    moduleMetadata({
        ...defaultModule, providers: [
            {
                provide: INITIAL_PLAYLISTS,
                useValue: playlistsMock
            }
        ],
    }),
]


export const NoPlaylists = Template.bind({});
// More on args: https://storybook.js.org/docs/angular/writing-stories/args
NoPlaylists.args = {};
NoPlaylists.decorators = [
    moduleMetadata({
        ...defaultModule, providers: [
            {
                provide: INITIAL_PLAYLISTS,
                useValue: []
            }
        ],
    }),
]
