// also exported from '@storybook/angular' if you can deal with breaking changes in 6.1
import { moduleMetadata } from '@storybook/angular';
import { Story, Meta } from '@storybook/angular/types-6-0';
import { PlaylistsDetailsComponent } from '../../app/playlists/components/playlists-details/playlists-details.component';
import { SharedModule } from '../../app/shared/shared.module';

import '../../styles.scss'

// More on default export: https://storybook.js.org/docs/angular/writing-stories/introduction#default-export
export default {
    title: 'Playlists/Playlists Details',
    component: PlaylistsDetailsComponent,
    // More on argTypes: https://storybook.js.org/docs/angular/api/argtypes
    argTypes: {
        // backgroundColor: { control: 'color' },
    },
    decorators: [
        moduleMetadata({
            imports: [
                SharedModule
            ],
        }),
    ],
} as Meta;

// More on component templates: https://storybook.js.org/docs/angular/writing-stories/introduction#using-args
const Template: Story<PlaylistsDetailsComponent> = (args: PlaylistsDetailsComponent) => ({
    props: args,
});

export const Primary = Template.bind({});
// More on args: https://storybook.js.org/docs/angular/writing-stories/args
Primary.args = {
    playlist: {
        id: '123', name: 'Playlist', public: false, description: '',
    }
};

export const LongName = Template.bind({});
// More on args: https://storybook.js.org/docs/angular/writing-stories/args
LongName.args = {
    playlist: {
        id: '123', name: 'very long playlist title that wont wrap', public: false, description: '',
    }
};
