// also exported from '@storybook/angular' if you can deal with breaking changes in 6.1
import { moduleMetadata } from '@storybook/angular';
import { Story, Meta } from '@storybook/angular/types-6-0';
import { UiWidgetComponent } from '..';

// import '../../styles.scss'

// More on default export: https://storybook.js.org/docs/angular/writing-stories/introduction#default-export
export default {
    title: 'Ui Toolkit/Widget Details',
    component: UiWidgetComponent,
    // More on argTypes: https://storybook.js.org/docs/angular/api/argtypes
    argTypes: {
        // backgroundColor: { control: 'color' },
    },
    decorators: [
        moduleMetadata({
            imports: [
            ],
        }),
    ],
} as Meta;

// More on component templates: https://storybook.js.org/docs/angular/writing-stories/introduction#using-args
const Template: Story<UiWidgetComponent> = (args: UiWidgetComponent) => ({
    props: args,
});

export const Primary = Template.bind({});
// More on args: https://storybook.js.org/docs/angular/writing-stories/args
Primary.args = {
    playlist: {
        id: '123', name: 'Playlist', public: false, description: '',
    }
};

export const LongName = Template.bind({});
// More on args: https://storybook.js.org/docs/angular/writing-stories/args
LongName.args = {
    playlist: {
        id: '123', name: 'very long playlist title that wont wrap', public: false, description: '',
    }
};
