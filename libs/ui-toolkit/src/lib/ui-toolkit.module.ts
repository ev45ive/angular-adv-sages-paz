import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UiWidgetComponent } from './ui-widget/ui-widget.component';

@NgModule({
  imports: [CommonModule],
  declarations: [
    UiWidgetComponent
  ],
  exports: [
    UiWidgetComponent
  ],
})
export class UiToolkitModule {}
