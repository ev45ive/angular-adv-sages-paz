module.exports = {
  "stories": [
    "../libs/ui-toolkit/src/stories/**/*.stories.mdx",
    "../libs/ui-toolkit/src/stories/**/*.stories.@(js|jsx|ts|tsx)",
    "../apps/sages/src/stories/**/*.stories.mdx",
    "../apps/sages/src/stories/**/*.stories.@(js|jsx|ts|tsx)"
  ],
  "addons": [
    "@storybook/addon-links",
    "@storybook/addon-essentials"
  ],
  "core": {
    "builder": "webpack5"
  }
}