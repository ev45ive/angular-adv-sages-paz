# GIT
cd ..
git clone https://bitbucket.org/ev45ive/angular-adv-sages-paz.git angular-adv-sages-paz
cd angular-adv-sages-paz
npm i
npm start

## GIT update
git stash -u 
git pull 
<!-- lub -->
git pull -u -f origin master

# Instalacje

node -v
v14.17.0

npm -v
6.14.6

git --version
git version 2.31.1.windows.1

## Powershell

ng.cmd --version

ng --version
Angular CLI: 12.2.10
Node: 14.17.0
Package Manager: npm 6.14.6

Google Chrome 94.0.4606.81

npm i -g @angular/cli

## Nx workspaces

npm install -g nx
npx create-nx-workspace --preset=angular

npx create-nx-workspace sages --preset=angular
√ Application name · sages
√ Default stylesheet format · scss
√ Use Nx Cloud? (It's free and doesn't require registration.) · No

> NX Nx is creating your workspace.

To make sure the command works reliably in all environments, and that the preset is applied correctly,
Nx will run "npm install" several times. Please wait.

✔ Installing dependencies with npm

## Extensions

Language Service - instelisense
https://marketplace.visualstudio.com/items?itemName=Angular.ng-template

Nx console CLI
https://marketplace.visualstudio.com/items?itemName=nrwl.angular-console

Switch HTML <> TS Alt+I/O/P
https://marketplace.visualstudio.com/items?itemName=adrianwilczynski.switcher

Code formatter
https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode

Types generator
https://marketplace.visualstudio.com/items?itemName=quicktype.quicktype

## Start app

nx s
npm run start

## UI toolkits

https://material.angular.io/guide/schematics

ng add @angular/material
? Choose a prebuilt theme name, or "custom" for a custom theme: Indigo/Pink [
Preview: https://material.angular.io?theme=indigo-pink ]
? Set up global Angular Material typography styles? Yes
? Set up browser animations for Angular Material? Yes

https://mdbootstrap.com/docs/standard/layout/grid/
https://www.primefaces.org/primeng/showcase/#/primeflex/grid

https://www.ag-grid.com/
https://getbootstrap.com/
https://ng-bootstrap.github.io/#/components/dropdown/examples
https://www.telerik.com/kendo-angular-ui <- nie polecam ciezkie

## Layout

https://github.com/angular/flex-layout
npm i -s @angular/flex-layout @angular/cdk

## Structure

- Core modules - providers, business, api 
- Shared modules - components/directives
- Feature modules

npx ng generate @schematics/angular:module --name=core --module=app --no-commonModule --no-interactive
npx ng generate @schematics/angular:module --name=playlists --module=app --route=playlists --routing --no-interactive
npx ng generate @schematics/angular:module --name=browse-music --module=app --route=music --routing --no-interactive
npx ng generate @schematics/angular:module --name=shared --module=app    --no-interactive


## Navigation
 npx ng generate @angular/material:navigation --name=shared/containers/navigation --module=shared --style=scss --export --routing --no-interactive


## Playlists

ng g c playlists/containers/playlists-view --type container
ng g c playlists/components/playlists-list
ng g c playlists/components/playlists-details
ng g c playlists/components/playlists-editor

## Storybook
https://nx.dev/l/a/storybook/overview#storybook

npx sb init
npm run storybook


 npx ng generate @nrwl/angular:storybook-configuration sages --no-configureCypress --no-interactive 


https://storybook.js.org/docs/angular/get-started/install
https://nx.dev/l/a/storybook/overview#storybook

https://docs.cypress.io/


## Immutable + onPush

https://immutable-js.com/ 
https://immerjs.github.io/immer/


## Playlists service

ng g s playlists/services/playlists
ng g s core/api/playlists-api


## Album search service

ng g i core/model/album
ng g s core/api/search-api
ng g s browse-music/services/music-search-mock
ng g s browse-music/services/music-search
ng g c browse-music/containers/album-search
ng g c browse-music/components/search-form
ng g c browse-music/components/search-results
ng g c browse-music/components/album-card


## Data validation, transfomation, etc..

// https://github.com/colinhacks/zod
// https://github.com/gvergnaud/ts-pattern
// https://github.com/typestack/class-transformer
// https://github.com/typestack/class-validator
// https://www.typescriptlang.org/docs/handbook/2/narrowing.html


## Shared library

> Executing task: npx ng generate @nrwl/angular:library --name=ui-toolkit --buildable --importPath=@sages/ui --prefix=ui --publishable --no-interactive --dry-run <

UPDATE angular.json
UPDATE nx.json
UPDATE package.json
CREATE libs/ui-toolkit/ng-package.json
CREATE libs/ui-toolkit/package.json
CREATE libs/ui-toolkit/README.md
CREATE libs/ui-toolkit/tsconfig.lib.json
CREATE libs/ui-toolkit/tsconfig.lib.prod.json
CREATE libs/ui-toolkit/tsconfig.spec.json
CREATE libs/ui-toolkit/src/index.ts
CREATE libs/ui-toolkit/src/lib/ui-toolkit.module.ts
CREATE libs/ui-toolkit/tsconfig.json
UPDATE tsconfig.base.json
CREATE libs/ui-toolkit/jest.config.js
CREATE libs/ui-toolkit/src/test-setup.ts
UPDATE jest.config.js
CREATE libs/ui-toolkit/.eslintrc.json

nx affected --target=build
nx affected --target=test
nx dep-graph

## Testing libraries
jasmine / jest

https://github.com/ngneat/spectator
https://material.angular.io/cdk/test-harnesses/overview
https://testing-library.com/docs/angular-testing-library/examples


## Global immutable reactive state store

https://ngrx.io/guide/store
https://www.ngxs.io/getting-started/installation

